extern crate rayon;

#[allow(unused_imports)]
use rayon::prelude::*;
use std::cmp;
use std::collections::HashMap;
use std::fs;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

macro_rules! mask {
    ($n:expr) => {
        ((1 << $n) - 1)
    }
}

const MORSE_MAP: &'static [(char, &str)] = &[
    ('a', ".-"),
    ('b', "-..."),
    ('c', "-.-."),
    ('d', "-.."),
    ('e', "."),
    ('f', "..-."),
    ('g', "--."),
    ('h', "...."),
    ('i', ".."),
    ('j', ".---"),
    ('k', "-.-"),
    ('l', ".-.."),
    ('m', "--"),
    ('n', "-."),
    ('o', "---"),
    ('p', ".--."),
    ('q', "--.-"),
    ('r', ".-."),
    ('s', "..."),
    ('t', "-"),
    ('u', "..-"),
    ('v', "...-"),
    ('w', ".--"),
    ('x', "-..-"),
    ('y', "-.--"),
    ('z', "--.."),
];

fn main() {
    easy_bonus_5("enable1.txt");
    //intermediate_bonus_1("smorse2-bonus1.txt");
}

#[allow(dead_code)]
fn easy_bonus_5(path: &str) {
    let vals = get_key_map();
    let wordlist =
        fs::read_to_string(path).expect(format!("Could not read file: {}", path).as_str());

    let mut all_vec = Vec::with_capacity(1 << 14);
    for _i in 0..(1 << 14) {
        all_vec.push(AtomicBool::new(true));
    }

    let all_ref = Arc::new(&all_vec);

    wordlist.par_lines().for_each(|line| {
        let morse: (u128, usize) = line.chars().fold((0, 0), |(val, len), c| {
            let index = (c as usize) - ('a' as usize);
            let bits = ((vals[index] >> 3) as u128, vals[index] & mask!(3));
            ((val << bits.1) | bits.0, len + bits.1)
        });
        if morse.1 >= 13 {
            let r = Arc::clone(&all_ref);
            for i in 0..=(morse.1 - 13) {
                let key = (1 << 13) | ((morse.0 >> i) & mask!(13));
                r[key as usize].store(false, Ordering::Relaxed);
            }
        }
    });

    for (i, e) in all_ref.iter().enumerate().skip(8192).take(8192) {
        if e.load(Ordering::Relaxed) {
            for j in (0..13).rev() {
                if ((i >> j) & 1) != 0 {
                    print!("-");
                } else {
                    print!(".");
                }
            }
            println!("");
        }
    }
}

#[allow(dead_code)]
fn intermediate_bonus_1(path: &str) {
    let smalpahas = fs::read_to_string(path).expect("Could not read file");

    smalpahas.par_lines().for_each(|smalpha| {
        let mut bool_map = get_bool_map();
        let bit_morse: u128 = smalpha.chars().fold(0, |acc, c| {
            if c == '.' {
                return acc << 1;
            } else {
                return acc << 1 | 1;
            }
        });
        let mut v: Vec<usize> = Vec::new();
        recurse(bit_morse, 82, &mut bool_map, &mut v);
        let alfa_map = get_alfa_map();
        let s: String = v.iter().map(|key| alfa_map[*key as usize]).collect();
        let check = smorse(&s);
        assert_eq!(check, smalpha);
        println!("{}", s);
    });
}

fn recurse(ma: u128, size: usize, map: &mut Vec<bool>, v: &mut Vec<usize>) -> bool {
    if size == 0 {
        return true;
    }

    for i in 1..=cmp::min(4, size) {
        let key = (((ma as usize) & mask!(i)) << 3) | i;
        if map[key] {
            map[key] = false;
            if recurse(ma >> i, size - i, map, v) {
                v.push(key);
                return true;
            } else {
                map[key] = true;
            }
        }
    }
    return false;
}

fn get_key_map() -> Vec<usize> {
    MORSE_MAP.iter()
        .map(|(_letter, morse)| {
            let tmp: usize = morse.chars()
                .map(|c| if c == '.' { 0 } else { 1 })
                .fold(0, |acc, c| {
                    let len = acc & mask!(3);
                    ((acc & !mask!(3)) << 1) | (c << 3) | len + 1
                });
            tmp
    }).collect()
}

fn get_bool_map() -> Vec<bool> {
    let keymap = get_key_map();
    let mut bool_map = vec![false; 256];

    for key in keymap {
        bool_map[key] = true;
    }

    bool_map
}

fn get_alfa_map() -> Vec<char> {
    let keymap = get_key_map();
    let mut alfa_map = vec!['-'; 256];

    for (i, key) in keymap.iter().enumerate() {
        alfa_map[*key] = ((('a' as usize) + i) as u8) as char;
    }

    alfa_map
}

fn smorse(text: &str) -> String {
    let map: HashMap<char, &str> = MORSE_MAP.iter()
    .cloned()
    .collect();

    text.chars()
        .map(|c| *map.get(&c).unwrap())
        .collect::<String>()
}
